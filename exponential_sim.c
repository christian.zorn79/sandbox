#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <openssl/sha.h>
#include <argp.h>
#include <string.h>
#include <math.h>
/*
 * This program generates a user defined sample from an exponential
 * distribution with a user-defined success parameter (default = 1).  
 * The user can choose the sample size (default = 10,000).
 *
 * The random number generator is an iterated SHA256 has where the digest
 * is treated as 4 64-bit words that are XOR'ed together.  The user can
 * provide a seed of up to 256 characters.  If not seeded by the yser, 
 * the program will read 256 bytes from /dev/urandom for a seed.
 */
static const int DEFAULT_SAMPLE_SIZE = 10000;
static const double DEFAULT_LAMBDA = 1.0;
static const int SEED_SIZE_BYTES = 256;
const char *argp_program_version = "exponential_sim 1.0";
const char doc[] = "A program to generate samples from an exponential distribution.";

// A struct to explain the command line arguments
static struct argp_option options[] = {
  { "seed", 's', "STRING", 0, "Seed for RNG (up to 256 chars)"},
  { "sample_size", 'S', "INT", 0, "Sample size (optional; default = 10000)"},
  { "lambda", 'l', "DOUBLE", 0, "Exponential dist. param w/ f(x) = lambda*exp(-lambda * x)."},
  {0}
};

// A struct to hold command line arguments
struct arguments
{
  char *seed;
  int sample_size;
  double lambda;
};


// The argp parser required by argp.h
static error_t parse_opt(int key, char *arg, struct argp_state *state){

  struct arguments *arguments = (struct arguments *)state -> input;
  switch(key){
  case 'S':
    arguments -> sample_size = atoi(arg);
    break;
  case 's':
    arguments -> seed = arg;
    break;
  case 'l':
    arguments -> lambda = atof(arg);
    break;
  default:
    return ARGP_ERR_UNKNOWN;
  }
  
  return 0;
}

static struct argp argp = { options, parse_opt, "", doc};

int main(int argc, char *argv[]){

  // The msg buffer will seed our PRNG (iterated SHA256)
  char seed_buffer[SEED_SIZE_BYTES];
  // Zero out the buffer; we want to get replicable results
  // for repeated input
  memset(&seed_buffer[0], 0x00, SEED_SIZE_BYTES);
  int sample_size; // Default sample size
  double lambda;
  
  // Instantiate an arguments struct
  struct arguments arguments;
  // Create default values; these will let us to case checking
  // after call to parse arguments
  arguments.seed = (char *)NULL;
  arguments.sample_size = -1;
  arguments.lambda = -1.0;
  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  // If arguments.seed != NULL, copy it into seed_buffer;
  // otherwise read form /dev/urandom
  if (arguments.seed != (char *) NULL){
    strncpy(seed_buffer, arguments.seed, SEED_SIZE_BYTES);
  }
  else{
    FILE *rand_file = fopen("/dev/urandom", "rb");
    if( rand_file == (FILE *)NULL ){
      fprintf(stderr, "Warning! Failed to open /dev/urandom to seed RNG.  ");
      fprintf(stderr, "RNG seed will be 256 bytes of 0x00.\n");
    }
    else{
      size_t bytes_read = fread(seed_buffer, sizeof(seed_buffer[0]), SEED_SIZE_BYTES, rand_file);
      if(bytes_read < SEED_SIZE_BYTES){
	fprintf(stderr, "Warning! Only read %ld bytes from /dev/urandom to seed RNG.\n",
		bytes_read);
      }
      fclose(rand_file);
    }
  }
  
  // If user input sample_size > 0, then overwrite the default
  sample_size = (arguments.sample_size > 0) ? arguments.sample_size : DEFAULT_SAMPLE_SIZE;

  // If the user provides a prob > 0 and < 1, you can use it
  lambda = (arguments.lambda > 0.0) ? arguments.lambda : DEFAULT_LAMBDA;
  
  // A message digest buffer for SHA256
  uint8_t md[32];
  // Hash the msg for a starting state for the prng
  SHA256(seed_buffer , SEED_SIZE_BYTES, md);

  // Create a pointer that will treat md as a uint64_t[4]
  uint64_t *a = (uint64_t*)md;

  // Create a variable to count the number of binomial "successes"
  // I.e., those for which random generation produces a double x
  // in [1.0,2.0] so that x < 1.0 + BINOM_P
  int pos_cnt = 0;

  double mean = 0.0;
  for(int i = 0; i < sample_size; ++i){ // Loop over the sample size
    // Step the PRNG
    SHA256(md, 32, md);
    // XOR the 4 64-bit words of md
    uint64_t aa = a[0] ^ a[1] ^ a[2] ^ a[3];
    // Shift off 12 low order bits and append 0x3ff to top 12 bits
    aa = 0x3ff0000000000000L | (aa >> 12);
    // Create a pointer to aa that will interpret those bits as
    // a double in [1.0,2.0]
    double *p = (double *)&aa;
    double x = -1.0*log(*p - 1.0)/lambda;
    mean += x;
    //fprintf(stdout, "%.8lf,",x);
  }
  mean /= ((double) sample_size);
  // Print output

  fprintf(stdout, "\n");
  fprintf(stdout, "Sample mean = %.8lf\n", mean);
  
  return 0;
  
}

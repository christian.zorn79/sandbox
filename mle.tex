\documentclass[10pt]{beamer}

\title{Introduction to Paramter Estimation}
\author{Christian Zorn}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}[fragile]
  \frametitle{Motivating Example}
  A new congenital risk factor is discovered for heart disease.  How prevalent is it?
  \vfill
  \onslide<2->\textbf{Idea:} Test a (representative) sample of the population to formulate an estimate.
  \vfill
  \onslide<3-> To simulate this:\\
  \verb+> make binomial_sim+\\
  \verb+> ./binomial_sim -u <username> [-t <optional tweet>]+\\
  Username and tweet seed a PRNG.
  \vfill
  \onslide<4-> Additional options:\\
  \verb+ [-s <sample size>] [-p <success probability>]+
\end{frame}

\begin{frame}[fragile]
  \frametitle{Simulation Results}
  I ran:\\
  \verb+> ./binomial_sim -u cazorn -t ``Hello World!''+\\
  \verb+Num positive cnt = 1722, Num Trials = 10000, p_hat = 0.17220000+
  \vfill
  \onslide<2->\textbf{Q:} What does the value $\hat{p}$ convey?
  \vfill
  \onslide<3->\textbf{A:} It is the proportion of our sample with the given risk factor.  It is also an estimate of the prevalence of the condition in the population.
  \vfill
  \onslide<4->\textbf{Q:} Is this a \textcolor{red}{good} estimate?  How does it \textcolor{red}{compare} to other estimates for $p$?
\end{frame}

\begin{frame}
  \frametitle{Likelihood Function}
  For one possible answer, we need to discuss the \textcolor{violet}{likelihood function}.
  \vfill
  \onslide<2-> \textbf{Model:} Each person in our sample is an \emph{independent} Bernoulli trial with success probability $p$.
  \vfill
  \onslide<3-> We don't know $p$, but are estimating it with $\hat{p}$.
  \vfill
  \onslide<4-> For any proposed probability of a positive result (denoted $\rho$), we can compute the probability of our observed under that assumed probability that a given case will be positive.  We will call $\rho$ a \emph{success probability}.
\end{frame}

\begin{frame}
  \frametitle{Likelihood Function (cont.)}
  Let $X = \{x_i\}_{i=0}^{N-1}$ be the result of our sample with $x_i=1$ for a positive result and $x_i=0$ for a negative result.  So the $x_i$ are independent, identically distributed [I.I.D] Bernoulli-$p$ random variables.
  \vfill
  Let $P_{\rho}(X)$ be the probability of observing $X$ if we assume that $p=\rho$.  Then
  \begin{align*}
    P_{\rho}(X)
    &= {N\choose t}\prod\limits_{i=0}^{N-1} \rho^{x_i}(1-\rho)^{1-x_i} \\
    &= {N\choose t}\rho^t(1-\rho)^{N-t}
  \end{align*}
where $t$ is the number of positive results (i.e., successes).
\vfill
\onslide<2-> For various reasons, we often look at the log probabilities, especially when we compute them explicitly (i.e., for numerical stability):
\[ \log(P_{\rho}(X)) = \log{N\choose t} + t\log(\rho) + (N-t)\log(1-\rho). \]
\end{frame}

\begin{frame}[fragile]
  \frametitle{Exploring values of $\rho$ for a given $X$}
  Let us take the results of our simulation and compute some log-probabilities for various $\rho$.
  \vfill
  \onslide<2-> For several values of $\rho$ try the following:\\
  \verb+> python3 binom_likelihood.py <num_trials> <num_successes> <rho>+
  \vfill
  \onslide<3-> Try this for different values of $\rho$, including $\rho=\hat{p}$.  What do you notice?
\end{frame}

\begin{frame}[fragile]
  \frametitle{Sample Output}
  In our simulation, we used a sample of size $10,000$ ($N=10,000$) and had $1722$ positive cases ($t = 1722$).
  \vfill
  \verb|> python3 binom_likelihood.py 10000 1722 .5|\\
  \verb|Log2-Likelihood ... = -3.37943648e+03|\\
  \verb|> python3 binom_likelihood.py 10000 1722 .3|\\
  \verb|Log2-Likelihood ... = -6.30127958e+02|\\
  \verb|> python3 binom_likelihood.py 10000 1722 .2|\\
  \verb|Log2-Likelihood ... = -4.27174293e+01|\\
  \verb|> python3 binom_likelihood.py 10000 1722 .1|\\
  \verb|Log2-Likelihood ... = -3.58078257e+02|\\
  \verb|> python3 binom_likelihood.py 10000 1722 .17|\\
  \verb|Log2-Likelihood ... = -6.81095284e+00|\\
  \verb|> python3 binom_likelihood.py 10000 1722 .1722|\\
  \verb|Log2-Likelihood ... = -6.56435986e+00|\\
  \vfill
  \onslide<2-> \textbf{Note:} You should get the maximum log-probability when $\rho=\hat{p}$.
\end{frame}

\begin{frame}
  \frametitle{Tying it All Together}
  Because our observations are fixed (we have a fixed sample).  We consider our log-probability as a function of $\rho$ (a function of possible success probabilities).  So we have
  \begin{align*}
    L_X(\rho) &:= P_{\rho}(X) = {N\choose t}\rho^t(1-\rho)^{N-t} \\
    \ell_X(\rho) &:= \log(P_{\rho}(X)) = \log{N\choose t} + t\log(\rho) + (N-t)\log(1-\rho).
  \end{align*}
  $L_X$ (resp. $\ell_X$) is simply the probability (resp. log-probability) thought of as a function of $\rho$ (the various success probabilities).
  \vfill
  $L_X$ (resp. $\ell_X$) is called the likelihood (resp. log-likelihood) function for our observations $X$.
  \vfill
  \onslide<2->\textbf{Important Note:} The function $L_X$ is determined by the set of observations $X=\{x_i\}_{i=0}^{N-1}$.  For a different set of observations $X'=\{x_{i}\}_{i=0}^{N'-1}$ we get a separate (but related) function $L_{X'}$.
  \vfill
  \onslide<3-> As we will show, $\hat{p}$ is a maximum value for $L_X(\rho)$ and $\ell_X(\rho)$.
\end{frame}

\begin{frame}
  \frametitle{A Little Calc I}
  $\ell_X(\rho)$ is a function of a continuous parameter $\rho$ with $0\leq\rho\leq 1$.
  \vfill
  In fact $\ell_X(\rho)$ is ``nice'' enough for $0 < \rho < 1$ to allow us to take derivatives with respect to $\rho$ and look for critical points.
  \vfill
  \[ \frac{d\ell_X}{d\rho} = \frac{t}{\rho} - \frac{N-t}{1-\rho} \]
  \vfill
  Setting $\frac{d\ell_X}{d\rho}=0$ and solving for $\rho$ yields $\rho = \frac{t}{N} = \hat{p}$.
  \vfill
  So $\hat{p}$ is a critical point for $\ell_X$.  One can take a second derivative to check that it is a local maximum.
\end{frame}  

\begin{frame}
  \frametitle{Layman's Explanation}
  \textbf{BLUF:} $\hat{p}$ is the success probability that makes your observations the most likely.
  \vfill
  \onslide<2-> In other words, the log-likelihood function $\ell_X(\rho)$ computes the log-probability of observing $X$ for any putative success probability $\rho$.
  \vfill
  \onslide<3-> When $\rho=\hat{p}$, $\ell_X$ is at a local maximum (in the case of binomial random variables, the maximum is global).  So $X$ is likeliest under the success probability $\rho=\hat{p}$.
  \vfill
  \onslide<4-> We call $\hat{p}$ a \textcolor{violet}{maximum likelihood estimator} for $X$.
  \vfill
  \onslide<4-> This idea extends to other families of distributions with continuous parameters (e.g. exponential, normal, Poisson, etc).
\end{frame}

\begin{frame}
  \frametitle{Other Examples of MLEs}
  In many cases, the MLE for a given distribution matches your intuition.
  \vfill
  \textbf{Ex. 1 - Multinomial Distribution}\\
  Let $X=\{x_i\}_{i=0}^{N-1}$ be a I.I.D. sample of random variables that can take on values from a finite set $S$.  For each $s\in S$, let $p_s = P(x=s)$ for $x$ sampled according to this distribution.  Then
\[ \hat{p}_s = \frac{\#\{x_i=s\mid 0\leq i\leq N-1\}}{N}. \]
\vfill
\textbf{Ex. 2 - Normal Distribution}\\
Let $X=\{x_i\}_{i=0}^{N-1}$ be and I.I.D sample of a normal random variable with mean $\mu$ and variance $\sigma^2$.  Then
\begin{align*}
  \hat{\mu} &= \frac{1}{N}\sum_{i=0}^{N} x_i =: \bar{x} \\
  \hat{\sigma} &= \sqrt{\frac{1}{N}\sum_{i=0}^{N}(x - \bar{x})^2}    
\end{align*}
\end{frame}

\begin{frame}
  \frametitle{Looking Ahead}
  \textbf{Q:} Great! But how do MLEs help me understand Hidden Markov Models [HMMs]?
  \vfill
  \onslide<2->\textbf{A:} Given a family of distributions or models $\{ \mathcal{M}(\lambda_1,\lambda_2,\ldots,\lambda_n) \}$ parameterized by the variables $\lambda_i$ and a set of observations $X=\{x_i\}_{i=0}^{N-1}$, we would like to answer the following three questions.
  \begin{enumerate}
  \item<3-> What is the probability of observing $X$ under the model $\mathcal{M}(\lambda_1,\lambda_2,\ldots,\lambda_n)$ for a given assignment of values for the $\lambda_i$?
  \item<4-> Given the model under two different assignments of parameters, $\mathcal{M}(\lambda_1,\lambda_2,\cdots,\lambda_n)$ and $\mathcal{M}(\lambda'_1,\lambda'_2,\cdots,\lambda'_n)$, how much more (or less) likely are our observations $X$ under one assignment of parameters than the other?
  \item<5-> Can we find and assignment of parameters $(\lambda_1,\lambda_2,\ldots,\lambda_n)$ that maximize the likelihood of observing $X$ in this family of models?
  \end{enumerate}
  \vfill
  \onslide<6-> For more complicated models (like HMMs), the answer to Question 3 is not a simple calculus question.
\end{frame}
  
\end{document}

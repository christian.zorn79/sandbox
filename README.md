# Sandbox

This project contains some programs and notes that I created during COVID-19 telework in fall 2020.
They include:

I.  Programs and notes for a statistics lecture introducing maximum likelihood estimates:
    A. binomial_sim.c - Source code for a binomial random variable simulator
    B. exponential_sim.c - Source code for an exponential random variable simulator
    C. binom_likelihood.py - A Python program to compute binomial log-probabilities
    D. mle.tex - LaTeX Beamer slides for an Intro to MLEs
    E. Makefile - For building C source code and typesetting LaTeX files
#!/usr/bin/env python3

from math import log, sqrt
import sys

if len(sys.argv) < 3:
    print("Usage: %s <observation file> <hidden state  file>" % sys.argv[0])
    sys.exit()


# Here we have some of the parameters for our HMM
NUM_STATES = 4
NUM_OUTS    = 3

# A dict to convert observation characters to ints
OBS_TO_INT = { 'R':0, 'B':1, 'G':2 }


# Here we have some of our matrices for HMMs

# PI[i] - P(X_0 = i), the init prob vector
PI_TRUE = [.25, .25, .25, .25]

# A[j][i] = P(X_{t+1} = i | X_t = j) which is
# the i,j-th entry of the transisiton matrix
A_TRUE = [ [ .25,  .25,  .25,  .25],
           [ .10,  .40,  .30,  .20],
           [.125, .125, .375, .375],
           [ .16,  .16,  .16,  .52] ]

# B[j][i] = P(O_t = i | X_t = j) which is
# the i,j-th entry of the output matrix
B_TRUE = [ [1.0/3.0, 1.0/3.0, 1.0/3.0],
           [    .50,     .25,     .25],
           [1.0/6.0, 1.0/2.0, 1.0/3.0],
           [    .30,     .30,     .40] ]


# Let write funcitons for the forward, backward, and Baum-Welch algorithms
def forward_algorithm(obs, PI, A, B):
    alpha = []
    sigma = []
    # Start with alpha_0
    alpha0 = [ B[i][obs[0]]*PI[i] for i in range(NUM_STATES) ]
    sigma0 = sum(alpha0)
    # Now normalize 
    for i in range(NUM_STATES):
        alpha0[i] /= sigma0
    # Now append to the respective lists
    alpha.append(alpha0)
    sigma.append(sigma0)
    # Now compute through the rest of the observations
    for t in range(1,len(obs)):
        alpha_new = [ 0.0 for i in range(NUM_STATES) ]
        # Compute the unnormalized alpha_new (NUM_STATES^2 work)
        for i in range(NUM_STATES):
            for j in range(NUM_STATES):
                alpha_new[i] += alpha[t-1][j]*A[j][i]
            alpha_new[i] *= B[i][obs[t]]
        # Normalize
        sigma_new = sum(alpha_new)
        for i in range(NUM_STATES):
            alpha_new[i] /= sigma_new
        alpha.append(alpha_new)
        sigma.append(sigma_new)
    return (alpha, sigma)

def backward_algorithm(obs, A, B, sigma):
    beta = []
    # Initialize beta0
    beta0 = [1.0 for i in range(NUM_STATES) ]
    beta.append(beta0)
    # Now compute through the rest of the observations
    for t in range(1,len(obs)):
        beta_new = [ 0.0 for i in range(NUM_STATES) ]
        # Compute the unnomralized beta_new (NUM_STATES^2 work)
        for i in range(NUM_STATES):
            for j in range(NUM_STATES):
                # Because we will insert beta_new to front of
                # beta, beta[0] is basically b_{t-1}
                beta_new[i] += beta[0][j]*A[i][j]*B[j][obs[len(obs)-t]]
            # Because we have the sigmas, we can normalize as we go
            beta_new[i] /= sigma[len(obs)-t]
        beta.insert(0,beta_new)
    return beta

def forward_backward_algorithm(obs, PI, A, B):
    (alpha, sigma) = forward_algorithm(obs, PI, A, B)
    beta = backward_algorithm(obs, A, B, sigma)
    gamma = [ [alpha[t][i]*beta[t][i]
               for i in range(NUM_STATES)]
              for t in range(len(obs)) ]
    return (alpha, beta, gamma, sigma)

def log_score(sigma):
    return -1.0* sum( [ log(s) for s in sigma ] ) / log(2)

def baum_welch_algorithm(alpha, beta, gamma, sigma, A, B):
    # Compute the digammas
    digamma = []
    for t in range(1,len(obs)):
        digamma_new = [ [ alpha[t-1][i]*A[i][j]*beta[t][j]*B[j][obs[t]]/sigma[t]
                          for j in range(NUM_STATES)]
                        for i in range(NUM_STATES) ]
        digamma.append(digamma_new)
    # Compute the new initial probabilites
    PI_NEW = [ gamma[0][i] for i in range(NUM_STATES) ]
    # Compute the new transition probabilities
    A_NEW  = []
    for i in range(NUM_STATES):
        denom = sum( [ gamma[t][i] for t in range(len(obs)-1) ] )
        A_ROW_NEW = [ sum([digamma[t][i][j] for t in range(len(obs)-1)])/denom
                      for j in range(NUM_STATES) ]
        A_NEW.append(A_ROW_NEW)
    # COmpute the new output probabilities
    B_NEW = []
    for i in range(NUM_STATES):
        denom = sum( [ gamma[t][i] for t in range(len(obs)) ] )
        B_ROW_NEW = [0.0 for j in range(NUM_OUTS)]
        for t in range(len(obs)):
            B_ROW_NEW[obs[t]] += gamma[t][i]
        for j in range(NUM_OUTS):
            B_ROW_NEW[j] /= denom
        B_NEW.append(B_ROW_NEW)
    return (PI_NEW, A_NEW, B_NEW)

# We create a version of Baum-Welch that leaves the first row
# of A and B fixed (for the case that we know those rows are flat)
def partial_baum_welch_algorithm(alpha, beta, gamma, sigma, A, B):
    # Compute the digammas
    digamma = []
    for t in range(1,len(obs)):
        digamma_new = [ [ alpha[t-1][i]*A[i][j]*beta[t][j]*B[j][obs[t]]/sigma[t]
                          for j in range(NUM_STATES)]
                        for i in range(NUM_STATES) ]
        digamma.append(digamma_new)
    # Compute the new initial probabilites
    PI_NEW = [ gamma[0][i] for i in range(NUM_STATES) ]
    # Compute the new transition probabilities
    A_NEW  = [[.25, .25, .25, .25]]
    for i in range(1,NUM_STATES):
        denom = sum( [ gamma[t][i] for t in range(len(obs)-1) ] )
        A_ROW_NEW = [ sum([digamma[t][i][j] for t in range(len(obs)-1)])/denom
                      for j in range(NUM_STATES) ]
        A_NEW.append(A_ROW_NEW)
    # COmpute the new output probabilities
    B_NEW = [[1.0/3.0, 1.0/3.0, 1.0/3.0]]
    for i in range(1,NUM_STATES):
        denom = sum( [ gamma[t][i] for t in range(len(obs)) ] )
        B_ROW_NEW = [0.0 for j in range(NUM_OUTS)]
        for t in range(len(obs)):
            B_ROW_NEW[obs[t]] += gamma[t][i]
        for j in range(NUM_OUTS):
            B_ROW_NEW[j] /= denom
        B_NEW.append(B_ROW_NEW)
    return (PI_NEW, A_NEW, B_NEW)


def find_sequence_of_most_likely_states(gamma):
    ml_states = []
    for g in gamma:
        ml_states.append( g.index(max(g)) )
    return ml_states

# Open and read in the observation file
OBS_FILE = sys.argv[1]
observations = open(OBS_FILE,"r").read()
# Convert the observations in to ints
obs = [ OBS_TO_INT[o] for o in observations ]

# Let us load the actual states and see how we did
HIDDEN_STATE_FILE = sys.argv[2]
actual_states = open(HIDDEN_STATE_FILE,"r").read()
actual_states = [ int(c) for c in actual_states ]

# # Let us create strawmen PI, A, B to see the Baum-Welch in action
PI = [.25, .25, .25, .25] # A flat prior on PI
# # For A, we suppose that we know the most probable transition for i->j for
# # each i, but not the corresponding probability
A  = [ [.25, .25, .25, .25],
       [.20, .40, .20, .20],
       [.20, .20, .40, .20],
       [.20, .20, .20, .40] ]
# # For B, we suppose that we know the most probably output for each state
# # but not the corresponding probability
B  = [ [1.0/3.0, 1.0/3.0, 1.0/3.0],
       [    .40,     .30,     .30],
       [    .30,     .40,     .30],
       [    .30,     .30,     .40] ]

(alpha, beta, gamma, sigma) = forward_backward_algorithm(obs, PI, A, B)
#print(alpha[-5:])
#print("\n\n")
#print(beta[-5:])
#print("\n\n")
#print(gamma[-5:])
#print("\n\n")
#print(sigma[-5:])
#print("\n\n")
print(log_score(sigma))

# Let us count the number of states that were guessed correctly
ml_states = find_sequence_of_most_likely_states(gamma)
count = 0
for t in range(len(ml_states)):
    if ml_states[t] == actual_states[t]:
        count += 1
print("Number of states guessed correctly: %d out of %d" % (count, len(ml_states)))
p   = (1.0/float(NUM_STATES))
q   = 1.0 - p
exp = p * len(ml_states)
z = (count - exp)/sqrt(q*exp)
print("Z-score for this number successes (H0 ~ Binom(%d,%f)) : %f"
      % (len(ml_states), p, z))

# Some tests
# 1.  We make sure that the sum of the alpha_t and gamma_t
# are equal to 1
def sum_test(arr):
    result = True
    for a in arr:
        s = sum(a)
        result = result and ( abs(s-1.0) <= 1.0e-12 )
    return result
print(sum_test(alpha))
print(sum_test(gamma))

for t in range(1200):
    (PI, A, B) = partial_baum_welch_algorithm(alpha, beta, gamma, sigma, A, B)
    (alpha, beta, gamma, sigma) = forward_backward_algorithm(obs, PI, A, B)
    if t % 100 == 99:
        print(log_score(sigma))

print(sum_test(A))
print(sum_test(B))
print("\n\n")
print(PI)
print("\n\n")
print(A)
print("\n\n")
print(B)

# Let us count the number of states that were guessed correctly
ml_states = find_sequence_of_most_likely_states(gamma)
count = 0
for t in range(len(ml_states)):
    if ml_states[t] == actual_states[t]:
        count += 1
print("Number of states guessed correctly: %d out of %d" % (count, len(ml_states)))
p   = (1.0/float(NUM_STATES))
q   = 1.0 - p
exp = p * len(ml_states)
z = (count - exp)/sqrt(q*exp)
print("Z-score for this number successes (H0 ~ Binom(%d,%f)) : %f"
      % (len(ml_states), p, z))

# Let us see how certain the algorithm is about correct guesses vs
# incorrect ones
correct_guess_probs   = []
incorrect_guess_probs = []
incorrect_guess_compar = []
for t in range(len(ml_states)):
    if ml_states[t] == actual_states[t]:
        correct_guess_probs.append(gamma[t][ml_states[t]])
    else:
        incorrect_guess_probs.append(gamma[t][ml_states[t]])
        incorrect_guess_compar.append( (gamma[t][ml_states[t]],
                                        gamma[t][actual_states[t]]) )

print("Among correct guesses max gamma = %f, min gamma = %f"
      % (max(correct_guess_probs), min(correct_guess_probs)) )
print("Among incorrect guesses max gamma = %g, min gamma = %f"
      % (max(incorrect_guess_probs), min(incorrect_guess_probs)) )

def find_worst_prob_diff(prob_compars):
    (p1_best,p2_best) = (.5,.5)
    for (p1,p2) in prob_compars:
        if abs(p1-p2) > abs(p1_best - p2_best):
            (p1_best,p2_best) = (p1,p2)
    return (p1_best,p2_best)

(p1,p2) = find_worst_prob_diff(incorrect_guess_compar)
print("Largest disparity for incorrect guess : (%f, %f, %f)"
      % (p1, p2, p2-p2) )

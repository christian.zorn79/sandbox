CC = gcc
CFLAGS = -std=c99
ifdef DEBUG
CFLAGS += -g
endif
ifdef OPTIMIZE
CFLAGS += -O3
endif

bayes_coin_sim:  bayes_coin_sim.c
	$(CC) $(CFLAGS) $< -o $@ -lcrypto

binomial_sim:  binomial_sim.c
	$(CC) $(CFLAGS) $< -o $@ -lcrypto

exponential_sim:  exponential_sim.c
	$(CC) $(CFLAGS) $< -o $@ -lm -lcrypto

hmm_sim:  hmm_sim.c
	$(CC) $(CFLAGS) $< -o $@ -lcrypto

mle.pdf: mle.tex
	pdflatex mle.tex
	pdflatex mle.tex

.PHONY:	all clean

all:
	make binomial_sim
	make exponential_sim
	make hmm_sim
	make mle.pdf

clean:
	rm -rf binomial_sim exponential_sim hmm_sim
	rm -rf *.out *.aux *.log *.nav *.pdf *.snm *.toc *.vrb *~

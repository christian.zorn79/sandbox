import math
import sys

# A function to approximate log(nCk) for large
# n and k using Sterling's Approximation
def approx_log_comb(n,k):
    a = n
    b = k
    c = n-k
    la = math.log(a)
    lb = math.log(b)
    lc = math.log(c)
    return a*la - b*lb - c*lc -.5*(lb + lc + math.log(2*math.pi) - la)

# A function for computing log-likelihood for
# binom(n,p) given n, p, and number of successes, t
# Returns log_2 of this likelihood
def approx_log_likelihood(n, p, n_success):
    q = 1.0-p
    t = n_success
    ll = approx_log_comb(n, t)+ t*math.log(p) + (n-t)*math.log(q)
    return ll/math.log(2)

def main():
    if len(sys.argv) < 4:
        print("Usage: %s <num_trials> <num_successes> <prob_sucess>\n" % sys.argv[0])
        return

    n = int(sys.argv[1])
    t = int(sys.argv[2])
    p = float(sys.argv[3])
    log2_likelihood = approx_log_likelihood(n, p, t)

    print("Log2-Likelihood for %d success on %d trials with success probability %.8lf = %.8le" % (n, t, p, log2_likelihood))

    return

if __name__ == "__main__":
    main()

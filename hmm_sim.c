#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>
#include<argp.h>
#include<openssl/sha.h>

static const int SEED_SIZE_BYTES = 256;
static const int SHA256_DIGEST_SIZE_BYTES = 32;
static const char OUTPUT_STATES[3] = { 'R', 'B', 'G' };
static const int   DEFAULT_NUM_OBS = 2400;
static const char *DEFAULT_OBS_FILE = "hmm_obs.out";
static const char *DEFAULT_STATE_FILE = "hmm_states.out";

/* 
 * STATE_TRANSITIONS[j][i] = P(X_{t+1} = i | X_t = j)
  * For this particular transition matrix we have
 *
 *  A = (a_{i,j})
 *
 *      | .25 .10 .125 .16 |
 *  A = | .25 .40 .125 .16 |
 *      | .25 .30 .375 .16 |
 *      | .25 .20 .375 .52 |
 */
static const double STATE_TRANSITIONS[4][4] = {
  {  .25,  .25,  .25,  .25 },
  {  .10,  .40,  .30,  .20 },
  { .125, .125, .375, .375 },
  {  .16,  .16,  .16,  .52 }
};

/* 
 * OUTPUT_PROBS[j][i] = P(O_t = i | X_t = j)
 * For this particular transition matrix we have
 *
 *  B = (b_{i,j})
 *
 *      | 1/3 .50 1/6 .3 |
 *  B = | 1/3 .25 1/2 .3 |
 *      | 1/3 .25 1/3 .4 |
 */
static const double OUTPUT_PROBS[4][3] = {
  { 1.0/3.0, 1.0/3.0, 1.0/3.0 },
  {     .50,     .25,     .25 },
  { 1.0/6.0, 3.0/6.0, 2.0/6.0 },
  {     .30,     .30,     .40 }
};
  
struct hmm_state_t {
  double *trans_prob;
  double *output_prob;
};

double generate_unif(char state[SHA256_DIGEST_SIZE_BYTES]);
int state_update(double uinf_rand, double *trans_probs);
char generate_output(double unif_rand, double output_probs[3]);

const char *argp_program_version = "hmm_sim 1.0";
const char doc[] = "A program to generate samples from a Hidden Markov Model.";

// A struct to explain the command line arguments
static struct argp_option options[] = {
  { "seed", 'S', "STRING", 0, "A seed for our RNG (up to 256 chars)" },
  { "obs_file", 'o', "STRING", 0, "An output file for observed values of HMM" },
  { "state_file", 's', "STRING", 0, "An output file for the hidden states"},
  { "num_obs", 'n', "INT", 0, "Number of observations (default = 2400)."},
  {0}
};

// A struct to hold command line arguments
struct arguments
{
  char *seed;
  char *obs_file;
  char *state_file;
  int num_obs;
};


// The argp parser required by argp.h
static error_t parse_opt(int key, char *arg, struct argp_state *state){

  struct arguments *arguments = (struct arguments *)state -> input;
  switch(key){
  case 'S':
    arguments -> seed = arg;
    break;
  case 'o':
    arguments -> obs_file = arg;
    break;
  case 's':
    arguments -> state_file = arg;
    break;
  case 'n':
    arguments -> num_obs = atoi(arg);
    break;
  default:
    return ARGP_ERR_UNKNOWN;
  }
  
  return 0;
}

static struct argp argp = { options, parse_opt, "", doc};

int main(int argc, char *argv[]){

  uint8_t seed_buffer[SEED_SIZE_BYTES];
  uint8_t buffer[SHA256_DIGEST_SIZE_BYTES];
  memset(buffer, 0x00, SHA256_DIGEST_SIZE_BYTES);

  // Instantiate an arguments struct
  struct arguments arguments;
  // Create default values; these will let us to case checking
  // after call to parse arguments
  arguments.seed = (char *)NULL;
  arguments.obs_file = (char *)NULL;
  arguments.state_file = (char *)NULL;
  arguments.num_obs = 0;
  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  if(arguments.seed != (char *)NULL){
    // If argc > 1 take user input as rand seed
    strncpy(seed_buffer, arguments.seed, SEED_SIZE_BYTES);
  }
  else{
    // Otherwise, read from /dev/urandom
    FILE *rand_file = fopen("/dev/urandom","rb");
    if( rand_file == (FILE*)NULL ){
      fprintf(stderr, "Warning!  Failed to open /dev/urandom.  ");
      fprintf(stderr, "RNG seeded with 256 bytes of 0x00!\n");
    }
    else{
      size_t bytes_read = fread(seed_buffer,
				sizeof(buffer[0]),
				SEED_SIZE_BYTES,
				rand_file);
      if( bytes_read != SEED_SIZE_BYTES ){
	fprintf(stderr, "Warning! Only read %ld bytes from /dev/urandom", bytes_read);
	fprintf(stderr, "to seed RNG!\n");
      }
      fclose(rand_file);
    }
  }

  FILE *obs_file = fopen( (arguments.obs_file != (char *) NULL)
			  ? arguments.obs_file
			  :  DEFAULT_OBS_FILE, "w" );
  if (obs_file == (FILE*)NULL){
    fprintf(stderr, "Error! Couldn't open observation file to write. Aborting!\n");
    return -1;
  }
  FILE *state_file = fopen( (arguments.state_file != (char *)NULL)
			    ? arguments.state_file
			    : DEFAULT_STATE_FILE, "w");
  if (state_file == (FILE*)NULL){
    fprintf(stderr, "Error! Couldn't open hiddens state file to write. Aborting!\n");
    fclose(obs_file);
    return -2;
  }

  int num_obs = (arguments.num_obs != 0)
    ? arguments.num_obs
    : DEFAULT_NUM_OBS ;

  
  
  // Hash buffer to as a starting state
  SHA256(seed_buffer, SHA256_DIGEST_SIZE_BYTES, buffer);

  // Setup HMM states
  struct hmm_state_t states[4];
  for(int i = 0; i < 4; ++i){
    states[i].trans_prob  = (double *)&STATE_TRANSITIONS[i][0];
    states[i].output_prob = (double *)&OUTPUT_PROBS[i][0];
  }

  // Setup output buffer
  char output_obs[num_obs+1];
  memset(output_obs, 0x00, num_obs);

  char state_sequence[num_obs+1];
  memset(state_sequence, 0x00, num_obs*sizeof(state_sequence[0]));
  
  // Determine an initial state
  double init_prob[4] = {.25, .50, .75, 1.0};
  double unif = generate_unif(buffer);
  int state_ind = 0;
  while( init_prob[state_ind] < unif ){
    ++state_ind;
  }
  // At this point we have an initial state
  // Now produce an inital output character
  state_sequence[0] = 0x30+state_ind;
  unif = generate_unif(buffer);
  output_obs[0] = generate_output(unif, states[state_ind].output_prob);
  // Now let us run through the HMM
  for(int t = 1; t < num_obs; ++t){
    unif = generate_unif(buffer); // Generate unif in U(0,1)
    state_ind = state_update(unif, states[state_ind].trans_prob);
    state_sequence[t] = 0x30+state_ind;
    unif = generate_unif(buffer);
    output_obs[t] = generate_output(unif, states[state_ind].output_prob);
  }

  // Write output
  fwrite(output_obs, sizeof(output_obs[0]), num_obs, obs_file);
  fwrite(state_sequence, sizeof(state_sequence[0]), num_obs, state_file);
  fclose(obs_file);
  fclose(state_file);
  
  return 0;
} 

double generate_unif(char state[SHA256_DIGEST_SIZE_BYTES]){
  // Update the state buffer
  SHA256(state, SHA256_DIGEST_SIZE_BYTES, state);
  // Extract bits for a double
  uint64_t *word_ptr = (uint64_t*)&state[0];
  uint64_t rand_word = word_ptr[0] ^ word_ptr[1] ^ word_ptr[2] ^ word_ptr[3];
  // Shift bits so rand_word will represent a float between [1,2]
  rand_word = (0x3ffL << 52) | (rand_word >> 12);
  double *rand_double = (double *)&rand_word;
  return (*rand_double) - 1.0;
}

int state_update(double unif_rand, double trans_probs[4]){
  int state = 0;
  while (trans_probs[state] < unif_rand){
    unif_rand -= trans_probs[state++];
  }
  return state;
}

char generate_output(double unif_rand, double output_probs[3]){
  int ind = 0;
  while( output_probs[ind] < unif_rand ){
    unif_rand -= output_probs[ind++];
  }
  return OUTPUT_STATES[ind];
}

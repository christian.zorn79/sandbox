#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <openssl/sha.h>
#include <argp.h>
#include <string.h>

/*
 * This program generates a series of Bernoulli-p trials for a given p
 * and updates a Bayes distribution on the probability distribution on the 
 * parameter space for p starting with a naive prior.  The set of possible
 * p are { .05t for 0 <= t <= 20 }
 *
 * The random number generator is an iterated SHA256 has where the digest
 * is treated as 4 64-bit words that are XOR'ed together.  The user can
 * provide a seed of up to 256 characters.  If not seeded by the user,
 * the program will read 256 bytes from /dev/urandom.
 */
static const double DEFAULT_HEAD_PROB = 0.30; // A default head prob
static const int DEFAULT_SAMPLE_SIZE = 1000;
static const int SEED_SIZE_BYTES = 256;
static const int N = 50;
const char *argp_program_version = "bayes_com_sim 1.0";
const char doc[] = "A program to generate samples from a Bernoulli distribution and then estimate the probability of a heads from it";

// A struct to explain the command line arguments
static struct argp_option options[] = {
  { "seed", 's', "STRING", 0, "Seed for RNG (up to 256 chars)"},
  { "sample_size", 'S', "INT", 0, "Sample size (optional; default = 100000)"},
  { "success_prob", 'p', "DOUBLE", 0, "Heads probability (default = .3)."},
  {0}
};

typedef enum coin_side { head = 0, tail = 1 } coin_side;

// A struct to hold command line arguments
struct arguments
{
  char *seed;
  int sample_size;
  double prob;
};


// The argp parser required by argp.h
static error_t parse_opt(int key, char *arg, struct argp_state *state){

  struct arguments *arguments = (struct arguments *)state -> input;
  switch(key){
  case 'S':
    arguments -> sample_size = atoi(arg);
    break;
  case 's':
    arguments -> seed = arg;
    break;
  case 'p':
    arguments -> prob = atof(arg);
    break;
  default:
    return ARGP_ERR_UNKNOWN;
  }
  
  return 0;
}

static struct argp argp = { options, parse_opt, "", doc};

coin_side flip_coin(uint8_t rand_buffer[32], double head_prob);
void print_prior(const double prior[N+1]) ;

int main(int argc, char *argv[]){

  // The msg buffer will seed our PRNG (iterated SHA256)
  char seed_buffer[SEED_SIZE_BYTES];
  // Zero out the buffer; we want to get replicable results
  // for repeated input
  memset(&seed_buffer[0], 0x00, SEED_SIZE_BYTES);
  int sample_size; // Default sample size
  double prob;
  
  // Instantiate an arguments struct
  struct arguments arguments;
  // Create default values; these will let us to case checking
  // after call to parse arguments
  arguments.seed = (char *)NULL;
  arguments.sample_size = -1;
  arguments.prob = -1.0;
  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  // If arguments.seed != NULL, copy it into seed_buffer;
  // otherwise read form /dev/urandom
  if (arguments.seed != (char *) NULL){
    strncpy(seed_buffer, arguments.seed, SEED_SIZE_BYTES);
  }
  else{
    FILE *rand_file = fopen("/dev/urandom", "rb");
    if( rand_file == (FILE *)NULL ){
      fprintf(stderr, "Warning! Failed to open /dev/urandom to seed RNG.  ");
      fprintf(stderr, "RNG seed will be 256 bytes of 0x00.\n");
    }
    else{
      size_t bytes_read = fread(seed_buffer, sizeof(seed_buffer[0]), SEED_SIZE_BYTES, rand_file);
      if(bytes_read < SEED_SIZE_BYTES){
	fprintf(stderr, "Warning! Only read %ld bytes from /dev/urandom to seed RNG.\n",
		bytes_read);
      }
      fclose(rand_file);
    }
  }

  // If user input sample_size > 0, then overwrite the default
  sample_size = (arguments.sample_size > 0) ? arguments.sample_size : 3000;

  // If the user provides a prob > 0 and < 1, you can use it
  prob = ((arguments.prob > 0.0) && (arguments.prob < 1.0)) ? arguments.prob : DEFAULT_HEAD_PROB;
  
  // A message digest buffer for SHA256
  uint8_t md[32];
  // Hash the msg for a starting state for the prng
  SHA256(seed_buffer, SEED_SIZE_BYTES, md);

  // Create buffers for all of the Bayes computations
  double prior[N+1];
  for(int i = 0; i <= N; ++i) {
    prior[i] = 1.0/((double) N+1);
  }
  double likelihood[N+1][2];
  for(int i = 0; i <= N; ++i) {
    likelihood[i][head] = ((double) i)/((double) N);
    likelihood[i][tail] = 1- likelihood[i][head];
  }
  double posterior[N+1];
  for(int i = 0; i <= N; ++i){
    posterior[i] = 0.0;
  }
  double evidence = 0.0;

  // Now we run the trials
  for(int i = 0; i < sample_size; ++i){
    if( i % 100 == 0 ){
      fprintf(stdout, "Prior after %d flips: ", i);
      print_prior(prior);
      fprintf(stdout,"\n");
    }
    // Now we update the distribution from data
    // 1. Flip a coin
    coin_side side = flip_coin(md, prob);
    // 2. Reset evidence
    evidence = 0.0;
    // 3. Update the posterior odds
    for(int t = 0; t <= N; ++t){
      posterior[t] = likelihood[t][side]*prior[t];
      evidence    += posterior[t];
    }
    // 4. Normalize the posterior odds
    for(int t = 0; t < 21; ++t){
      posterior[t] /= evidence;
    }
    // 5. Copy posterior into prior
    memcpy(prior, posterior, (N+1)*sizeof(double));
  }
  fprintf(stdout, "Prior after %d flips: ", sample_size);
  print_prior(prior);
  fprintf(stdout,"\n");

  return 0;
  
}

coin_side flip_coin(uint8_t rand_buffer[32], double head_prob){
  // Update rand_buffer by hashing it
  SHA256(rand_buffer, 32, rand_buffer);
  // Create a uint64_t to aggreage
  uint64_t rand = 0;
  // Cast rand_buffer to a uint64_t ptr
  uint64_t *ptr64 = (uint64_t *)&rand_buffer[0];
  // Xor the 4 64-bit words into rand
  rand = ptr64[0] ^ ptr64[1] ^ ptr64[2] ^ ptr64[3];
  // Set the top bits of rand so it represents a float between 1 and 2
  rand = 0x3ff0000000000000L | (rand >> 12);
  // Cast this to a double
  double *prob_ptr = (double *)&rand;
  // Return resutl
  return (*prob_ptr <= (1.0 + head_prob)) ? head : tail;
}

void print_prior(const double prior[N+1]) {
  for(int t = 0; t <= N; ++t){
    fprintf(stdout, "%.6lf ", prior[t]);
  }
}
